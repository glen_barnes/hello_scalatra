FROM tomcat

MAINTAINER Glen Barnes

RUN apt-get update && apt-get clean

COPY target/scalatra-maven-prototype.war /usr/local/tomcat/webapps/hello-scalatra.war

EXPOSE 8080

CMD ["catalina.sh", "run"]
